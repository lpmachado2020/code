# Protocolos para la transmisión de audio y video en Internet
# Práctica 4. Sockets UDP y registrar SIP

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta un 0.7 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de Python y de orientación a objetos (las de las prácticas anteriores). 
* Nociones de SIP (las vistas en clase de teoría)

**Tiempo estimado:** 10 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p4-21

**Fecha de entrega parte individual:** 5 de noviembre de 2021, 23:59 (hasta ejercicio 12, incluido)

**Fecha de entrega parte interoperación:** 7 de noviembre de 2021, 23:59 (ejercicio 13)

## Introducción

[`socketserver`](https://docs.python.org/3/library/socketserver.html) es un módulo en Python que simplifica la construcción de servicios en Internet. Aunque hay cuatro tipos diferentes de servidores básicos, nosotros en esta práctica utilizaremos solamente [`UDPServer`](https://docs.python.org/3/library/socketserver.html#socketserver.UDPServer), que utiliza datagramas UDP. Es importante recordar que los paquetes UDP pueden llegar desordenados, incluso se pueden perder por el camino (algo que no puede pasar con los flujos TCP que utliza `TCPServer`). `UDPServer` trabaja de manera secuencial, lo que significa que las peticiones serán atendidas de una en una, por lo que una petición tendrá que esperar si hubiera otra en proceso en ese momento.

## Objetivos de la práctica

* Manejar SIP de manera sencilla. 
* Crear un esquema cliente-servidor en Python.

## Ejercicio 1

Esta práctica requiere hacer capturas con la herramienta _wireshark_. Para que el administrador de sistemas te incluya en el grupo con permisos para poder hacerlo en las máquinas del laboratorio, abre un terminal e introduce esta instrucción:

```shell
touch $HOME/.ptavi2022
```

Esta instrucción crea un archivo vacío en tu cuenta. El administrador de sistema buscará por este archivo para incluir a los usuarios en el grupo con permisos para capturar con _wireshark_.}

## Ejercicio 2

Con el navegador, dirígete al [repositorio plantilla de esta práctica](https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p4-21) y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la ETSIT).

## Ejercicio 3

Estudia el código de un sencillo cliente UDP en `client.py` que encontrarás en este mismo directorio. Fíjate en que:

* Importa el módulo `socket`. 
* Inicializa varias "variables" constantes (nota que al ser constantes, vienen en mayúsculas, como marca la convención correspondiente en PEP8).
* Crea un `socket`.
* Lo prepara para enviar datos a un servidor (`connect()`).
* Envía una secuencia de bytes por el `socket` con el método `send()` y lee del `socket` con `recv()`.
* Al recibir con `recv()` indicamos el valor del `buffer` en bytes.
* No hace falta cerrar explícitamente la conexión, ya que se hace automáticamente al salir del contexto del `with`.

Modifica este programa para que envíe primero el mensaje `Hola`, escriba en pantalla lo que reciba del servidor, y después envíe el mensaje `Adiós`, y escriba en pantalla lo que reciba del servidor.

Incluye el programa en el repositorio de entrega.

## Ejercicio 4

Estudia el código de `server.py`, que implementa un servidor de respuesta basado en UDP. Fíjate en que:

* Importa el módulo [`socketserver`](https://docs.python.org/3/library/socketserver.html).
* Tenemos una única clase que manejará las peticiones.
* Esta clase hereda de una clase `DatagramRequestHandler` que hay en el módulo `socketserver`.
* La clase no tiene constructor `__init__`, utiliza el de la clase padre.
* La clase sólo tiene un método, llamado `handle()`.
* El método `handle()` se ejecuta cada vez que recibimos una petición en el servidor.
* `self.wfile` y `self.rfile` son los atributos que abstraen el `socket` (como si fuera un fichero):
  * Podemos leer del mismo, iterando sobre `self.rfile` como si fuera un fichero de lectura.
  * Podemos escribir en el mismo con `self.wfile.write()`.
* Enviamos y recibimos secuencias de bytes. 
* Un programa principal, donde se instancia la clase `EchoHandler`, indicando la IP y el puerto donde se deja al servidor escuchando en un bucle infinito (del que sólo se puede salir desde el terminal con _Ctrl+C_, que lanza una excepción `KeyboardInterrupt`.


## Ejercicio 5

_Nota_: Si te es fácil, renombra el programa a `client_basic.py` (si no, no te preocupes, no causará problemas con la corrección).

Cambia el programa cliente para que:

* Se pase como parámetro al programa la IP y el puerto del servidor, así como a continuación el mensaje que se ha de enviar. Nota que la shell (el intérprete de comandos) nos dará el mensaje como `string`, y tendremos que transformarlo en una secuencia de bytes. Para ejecutar el cliente, deberíamos hacer:

```shell
python3 client.py ip puerto linea

```

Una ejemplo de llamada sería: 

```shell
python3 client.py 127.0.0.1 5060 eco eco, soy yo
```

A continuación de enviar el mensaje, el programa escribirá en pantalla el mensaje que reciba del servidor.

Asegúrate de que en el programa, el porgrama principal está también en una función `main`, igual que el original. Incluye el programa en el repositorio de entrega.

Puedes probar tu programa utilizando `tests/test_client_basic.py` (este test asume que tu programa se llama `client_basic.py`):

```shell
python3 -m unittest tests/test_client_basic.py
```

## Ejercicio 6

_Nota_: Si te es fácil, renombra el programa a `server_basic.py` (si no, no te preocupes, no causará problemas con la corrección).

Modifica el programa del servidor para que:

* Se pase el puerto al que ha de escuchar como parámetro al programa.
* Al arrancar, muestre en pantalla `Server listening in port <port>`, siendo `<port>` el puerto indicado como parámetro.
* Cada vez que reciba un mensaje:
  * Muestre en pantalla la dirección IP y el puerto del cliente (esta información viene en el atributo `client_address` en forma de tupla), y el mensaje recibido. Por ejemplo, si se ha recibido el mensaje `Hola`, desde la dirección IP `127.0.0.1` y el puerto `5446`, se mostrará el mensaje `127.0.0.1 5446 Hola`
  * Responda con el mensaje `Message: <msg>`, donde `<msg>` es el mensaje recibido.

_Nota:_ Las tuplas son listas especiales, una especie de listas de solo lectura. Se definen con paréntesis, no con corchetes. Puedes leer [más sobre tuplas en Dive into Python](https://diveintopython3.net/native-datatypes.html#tuples).

Tras realizar el ejercicio 5 y 6, si un cliente envía el mensaje `Hola Pepe`, y luego `Adiós Pepe`, el servidor escribirá algo como:

```
Server listening in port 6001
127.0.0.1 47888 Hola Pepe
127.0.0.1 39655 Adiós Pepe
```

Asegúrate de que en el programa, el porgrama principal está también en una función `main`, igual que el original. Incluye el programa en el repositorio de entrega.

Puedes probar tu programa utilizando `tests/test_server_basic.py` (este test asume que tu programa se llama `server_basic.py`):

```shell
python3 -m unittest tests/test_server_basic.py
```

## Ejercicio 7

Modifica los programas anteriores para tener un cliente que envíe mensajes de registro SIP y un servidor _Registrar SIP_. El cliente se llamará 'client.py' y el servidor ser llamará 'server.py'. Cada vez que el cliente le mande una línea con el método `REGISTER` (en mayúsculas), el servidor guardará la dirección registrada y la IP en un diccionario. Este diccionario ha de ser un [atributo de clase, no de la instancia](http://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide). Cambia el nombre de la clase del servidor de `EchoHandler` a `SIPRegisterHandler`. La petición SIP del cliente tendrá una pinta parecido a lo siguiente:

```
REGISTER sip:luke@polismassa.com SIP/2.0\r\n\r\n
```

El servidor deberá responder con un mensaje de este estilo:

```
SIP/2.0 200 OK\r\n\r\n
```

El cliente se deberá seguir ejecutándose desde línea de comando, ahora de la siguiente manera:

```shell
python3 client.py <ip> <puerto> register luke@polismassa.com
```

## Ejercicio 8

Añade funcionalidad al cliente y al servidor para ofrecer la posibilidad de darse de baja a un usuario. Para ello, hay que añadir una cabecera `Expires` (cuyos valores vienen dados en segundos). En caso de que el valor de la cabecera `Expires` sea 0, el usuario será borrado del diccionario de registro. En cualquier otro caso, siempre que el tiempo sea positivo, el valor de `Expires` será el tiempo de expiración en el servidor. En este ejercicio no hace falta considerar funcionalidad de servidor para cuando el registro de un cliente caduca. La petición del cliente será parecida a ésta:

```
REGISTER sip:luke@polismassa.com SIP/2.0\r\n
Expires: 0\r\n\r\n
```

El servidor deberá borrar al usuario del diccionario y responder con un

```
SIP/2.0 200 OK\r\n\r\n
```

El cliente se deberá seguir ejecutando desde línea de comando, ahora de la siguiente manera:

```shell
python client.py localhost 2500 register luke@polismassa.com 3600
```

donde 3600 es un ejemplo del tiempo de expiración y será el valor de la cabecera `Expires`.

En caso de que no se introduzcan los valores necesarios, se imprimirá el siguiente mensaje:

```shell
Usage: client.py <ip> <puerto> register <sip_address> <expires_value>
```

## Ejercicio 9

Modifica el servidor para añadir el método `registered2json` en el que se implemente la siguiente funcionalidad: cada vez que un usuario se registre o se dé de baja, se imprimirá en el fichero `registered.json` con información sobre el usuario, su dirección y la hora de expiración. A continuación, se puede ver un ejemplo. Tu fichero JSON no tiene que ser exactamente igual que lo mostrado, simplemente ha de ser un fichero JSON válido con los datos de usuarios registrados legible por humanos (esto es, la fecha ha de ser entendible).

```json
[
  "luke@polismassa.com",
  {
    "address": "localhost",
    "expires": "2019-10-16 10:37:12 +0000"
  }
]
```

Para los formatos de tiempo, se recomienda utilizar el módulo time, en particular:
* `time()`, que devuelve los segundos desde el 1 de enero de 1970,
* `gmtime()`, que toma los segundos desde el 1 de enero de 1970 y te lo devuelve en una tupla,
* `strptime()`, que representa el tiempo en un `string`.

De esta manera,

```python
time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
```

devolverá un `string` con la hora GMT actual. Y

```python
time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(1233213))
```

devolverá un `string` con la hora GMT del segundo 1.233.213 desde el 1 de enero de 1970.

En este ejercicio no es necesario implementar funcionalidad para que el servidor `Registrar` gestione la caducidad de los usuarios registrados: sólo se pide que se borre a un usuario registrado si se recibe un mensaje `REGISTER` con tiempo de expiración 0. De todas formas, si se quiere, se puede implementar que cada vez que se reciba un mensaje, o cada vez que se vaya a actualizar el fichero JSON, se comprueben las fechas para detectar si alguna ha expirado, y en ese caso borrar al usuario registrado correspondiente.

## Ejercicio 10

Modifica el servidor, añadiendo el método `json2registered`, para que cuando se lance, compruebe si hay un fichero llamado `registered.json`. Si existe, se leerá su contenido y se usará como diccionario de usuarios registrados. Si da cualquier error al leer el fichero, el servidor se ejecutará como si el fichero JSON no existiera.

Es conveniente que este método sea un método de la clase 'SIPRegisterHandler'. De esta forma, podrás llamarlo desde el programa principal como `SIPRegisterHandler.json2registered()`. Puedes encontrar más información sobre cómo funcionan los métodos de clase, y cómo se comparan con los métodos de instancia y los métodos estáticos en [Instance, Class, and Static Methods — An Overview](https://realpython.com/instance-class-and-static-methods-demystified/#class-methods).

Puedes probar tus programas cliente y servidor utilizando `tests/test_client.py` y `tests/test_server.py`:

```shell
python3 -m unittest tests/test_client.py
python3 -m unittest tests/test_server.py
```

## Ejercicio 11

Documenta tu código con _docstrings_. Previamente, lee la [recomendación para _docstrings_ en Python](https://www.python.org/dev/peps/pep-0257/). Comprueba asimismo que los nombres de las variables siguen las indicaciones de PEP8. 

## Ejercicio 12

Realiza una captura con _wireshark_ (usando la interfaz de captura `localhost`) con las siguientes interacciones:

* El cliente `marty.mcfly@sk8ing.com` se registra. Tiempo de expiración: 5. 
* El cliente `doc@delorean.com` se registra. Tiempo de expiración: 3600.
* Se dejan pasar unos segundos, más de cinco.
* El cliente `doc@delorean.com` se da de baja.
* El cliente `marty.mcfly@sk8ing.com` se da de baja.

Investiga tu captura. Comprueba, en particular, que puedes ver el intercambio de mensajes y que todo va en texto claro por la red. Guarda la captura en un fichero de nombre `register.libpcap` y súbelo al repositorio git de entrega.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:

* Se valorará que haya realizado al menos haya ocho commits, correspondientes más o menos con los ejercicios pedidos, en al menos dos días diferentes, sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8 y PEP257.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorar ́a que los programas se invoquen exactamente según se especifica, y que muestren  mensajes y errores correctamente según se indica en el enunciado de la práctica.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Para muchos de los apartados del programa, se proporciona un test. Puedes ejecutarlo para ver que el test pasa. Ten en cuenta que el hecho de que pase el test no quiere decir que la parte correspondiente esté completamente bien, aunque los tests están diseñados para probar los errores más habituales. Recuerda que si el test es `test_xxx.py`, para ejecutarlo podrás ejecutarlo en PyCharm, o desde la línea de comandos:

```shell
python3 -m unittest tests/test_xxx.py
```

Cuando tengas la práctica lista, puedes realizar una prueba general, incluyendo la comprobación del estilo de acuerdo a PEP8, que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Ejercicio 13 (segundo periodo)

Elije el repositorio de uno de tus compañeros que hayan entregado la práctica. Ejecuta su registrar SIP sin fichero `registered.json`, y utiliza tu cliente para registrar dos direcciones distintas con tiempo de expiración de 3600 segundos, y a continuación registra una de ellas con tiempo de expiración 0 segundos (con lo que debería quedar eliminada).

Una vez lo hayas hecho, abre una incidencia (issue) en el repositorio de tu compañero, titulada `Prueba por <user>`, donde `<user>` es tu usuario en GitHub. Describe en ella cómo has probado el servidor, incluyendo el log de ejecución de tu cliente (las tres líneas con la que lo has ejecutado), los mensajes escritos por el servidor en respuesta a esas ejecuciones, y el fichero `registered.json` que queda después de la ejecución.
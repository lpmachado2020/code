# Protocolos para la transmisión de audio y video en Internet
# Proyecto final (segunda versión, junio 2022)

**Conocimientos previos necesarios:**

* Programación Python (practicas previas)
* Documentos XML y JSON (clases de teoria, prácticas previas)
* Uso de Wireshark (prácticas previas)
* Nociones de SIP (clases de teoría, prácticas previas)
* Nociones de RTP (clases de teoría, prácticas previas)

**Tiempo estimado:** 25 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-pfinal2-21

**Fecha de entrega (parte básica y adicional):** 17 de junio de 2022, 23:59

**Video de presentacion de detalles:** Para la práctica final de enero se grabó un video, que en gran medida es válido para esta práctica: [Video grabado el 22 de diciembre](https://urjc.sharepoint.com/:v:/s/gr_2021.2039_2039021_3AM/EaSKvs8lC1RKuzA-9Ddn52YBWlbyZxHz8O1TgTuscJOWKw?e=8zYXLa) (requiere autenticación). Pero ojo, esta práctica tiene algunos cambios: los detalles válidos son los que se incluyen en este enunciado.

## Introducción

Este proyecto tiene como objetivo implementar un servicio de emisión y recepción de canales de audio utilizando tecnología de VoIP (SIP y RTP, fundamentalmente). Para realizar el proyecto, el alumno tendrá que realizar tres programas:

* ServidorRegistrar. Un programa que actuará como _registrar_, que aceptará registro de direcciones SIP de servicios y clientes.
* ServidorProxy. Un programa que actuará como _proxy_, que aceptará las peticiones de inicio de sesión de clientes, reenviándolas a la otra parte (normalmente un servicio, en este proyecto).
* ServidorRTP. Un programa que implementará el servicio de emisión de canales de audio. El programa se registrará en el _registrar_ con una dirección SIP propia, y si un cliente establece una sesión (llamada) cón él (usando SIP), le enviará mediante RTP un fichero de audio.
* Cliente. Un programa que implementará el cliente. El programa, al arrancar, se registrará también en el _registrar_ con una dirección SIP propia (si no está registrado previamente, el _registrar_ no le hará caso), y luego establecerá una sesión (llamada) con uno de los servicios, para recibir el fichero de audio correspondiente.

En este esquema, tanto ServidorRTP como Cliente funcionan como UAs SIP, aunque sólo Cliente iniciará sesiones (enviará invitaciones) y sólo ServidorRTP enviará datos multimedia (como paquetes RTP), que recibirá Cliente.

La forma normal de funcionar de los cuatro programas será la siguiente:

* Se lanza un ServidorRegistrar.

* Se lanza un ServidorProxy.

* Se lanza al menos un ServidorRTP, que se registrará en el ServidorRegistrar con su dirección SIP, y quedará esperando invitaciones.

* Se lanza al menos un Cliente, que se registrará en el ServidorRegistrar con su dirección SIP, y que a continuación iniciará una sesión, enviando una invitación al ServidorProxy indicando la dirección SIP de un ServidorRTP. La invitación llevará un documento SDP indicando los parámetros de la sesión.

* El ServidorProxy reenviará la invitación al ServidorRTP.

* Si todo es correcto, el ServidorRTP responderá a la invitación aceptándola, para lo que enviará una respuesta OK al ServidorProxy, que a su vez se la reenviará al Cliente.

* El Cliente responderá con un ACK, que enviará a ServidorProxy, para que éste lo reenvíe al ServidorRTP. Tras hacerlo, quedará escuchando en el puerto donde espera recibir los paquetes RTP.

* Al recibir el ACK, el ServidorRTP comenzará a enviar los paquetes RTP correspondientes del fichero de audio que sirve directamente al Cliente, según los parámetros especificados en el documento SDP.

* Al cabo de un cierto tiempo el Cliente enviará un BYE al ServidorProxy, para que lo reenvíe al ServidorRTP. Al recibirlo, éste interrumpirá la transmisión de paquetes RTP y responderá on un OK, que llegará al Cliente cía el ServidorProxy.

* Al recibir el OK, el Cliente terminará, salvando previamente un fichero con el audio que ha recibido de ServidorRTP.

* Para poder hacer su trabajo, el ServidorRegistrar y el ServidorProxy compartirán los datos de clientes y servicios registrados (el ServidorRegistrar escribirá los datos en un fichero, y el ServidorProxy leerá los datos que necesite de él).

## Objetivos del proyecto

* Realizar un sistema SIP razonablemente realista.
* Poner en práctica lo aprendido en las prácticas anteriores.
* Probar la interacción de SIP y RTP en un entorno concreto.

## Puntuación

Esta documento describe el proyecto final de la asignatura (práctica final), que puede aportar hasta 2.75 puntos (sobre un total de 10) en la nota final de la asignatura. Esta puntuación se organiza como sigue:

* El funcionamiento correcto de la parte básica del proyecto supondrá que el proyecto final se considera como aprobado, pero aportará 0 puntos a la nota final de la asigntura. Tener esta parte funcionando correctamente es requisito necesario para poder aprobar la asignatura.
* El funcionamiento correcto de la parte adicional del proyecto podrá suponer hasta 2 puntos de la nota final de la asignatura.
* Además, habrá una parte a desarrollar colaborativamente, donde se probará la interacción entre prácticas de distintos alumnos, que podrá suponer hasta 0.75 puntos de la nota final de la asignatura.

## Comienzo

Con el navegador, dirígete al [repositorio plantilla de este proyecto](https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-pfinal2-21) y realiza un fork, de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona el repositorio que acabas de crear a local para poder editar los archivos. Trabaja a partir de ahora en ese repositorio, sincronizando los cambios que vayas realizando según los ejercicios que se comentan a continuación (haciendo commit y subiéndolo a tu repositorio en el GitLab de la ETSIT).

## Cliente (parte básica)

El cliente ha de ejecutarse así:

```shell
$ python3 client.py <IPReg>:<puertoReg> <IPProxy>:<puertoProxy> <dirCliente> <dirServidorRTP> <tiempo> <fichero>
```

donde:

* `<IPReg>` es la dirección IP de la máquina donde está ServidorRegistrar
* `<puertoReg>` es el puerto UDP donde está escuchando ServidorRegistrar
* `<IPProxy>` es la dirección IP de la máquina donde está ServidorProxy
* `<puertoProxy>` es el puerto UDP donde está escuchando ServidorProxy
* `<dirCliente>` es la dirección SIP del cliente, con la que se va a registrar en ServidorRegistrar.
* `<dirServidorRTP>` es la dirección SIP del servidor cn el que se va a iniciar una sesión para que nos envíe audio, tal y como la haya registrado el servidor en cuestión. Las direcciones tendrán que ser direcciones SIP válidas, por ejemplo `sip:heyjude@signasong.net`
* `<tiempo>` será el tiempo, en segundos, que el cliente mantendrá abierta la sesión una vez ésta haya empezado: al terminar este tiempo, el cliente enviará un `BYE` para terminar la sesión
* `<fichero>` será el fichero donde se guardará el audio recibido de ServidorRTP

Por ejemplo:

```shell
$ python3 client.py 127.0.0.1:6001 127.0.0.1:6002 sip:yo@clientes.net sip:heyjude@signasong.net 10 miaudio.mp3
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 client.py <IPReg>:<portReg> <IPProxy>:<portProxy> <addrClient> <addrServerRTP> <time> <file>
```

Al arrancar, el cliente enviará un `REGISTER` a ServidorRegistrar, para registrarse:

```
REGISTER sip:yo@clientes.net SIP/2.0\r\n\r\n
```

ServidorSIP deberá responder con un mensaje de este estilo:

```
SIP/2.0 200 OK\r\n\r\n
```

A continuación, el cliente se preparaá para recibir paquetes RTP, y enviará `INVITE` al ServidorProxy, indicando en un documento SDP el puerto UDP en que se ha preparado para escuchar RTP. Este documento SDP tendrá los siguientes elementos:

* `v`: versión, por defecto 0
* `o`: originador e identificador de sesión (la dirección del originador y su dirección IP,
* `s`: nombre de la sesión, que será el nombre del usuario SIP con el que se comunica
* `t`: tiempo que la sesión lleva activa, en nuestro caso, siempre 0
* `m`: tipo de elemento multimedia y puerto de escucha y protocolo de transporte utilizados, en esta práctica `audio`, el número de puerto pasado al programa principal como parámetro y ``RTP'').

Por ejemplo (suponiendo que el puerto 34543 es donde Cliente espera recibir los paquetes RTP):

```
INVITE sip:heyjude@signasong.net SIP/2.0
Content-Type: application/sdp

v=0
o=sip:yo@clientes.net 127.0.0.1
s=heyjude
t=0
m=audio 34543 RTP
```

El ciente quedará esperando el OK de ServidorRTP (recibido vía ServidorProxy), y cuando lo reciba, enviará un ACK a ServidorProxy:

```
ACK sip:heyjude@signasong.net SIP/2.0\r\n\r\n
```

Cuando haya pasado el tiempo especificado, Cliente enviará un BYE:

```
BYE sip:heyjude@signasong.net SIP/2.0\r\n\r\n
```

y cuando reciba el OK correspndiente, terminará, asegurándose antes de que se almacenan en el fichero especificado todas las muestras de sonido recibidas en paquetes RTP.

Para recibir los paquetes RTP desde `ServidorRTP` se puede usar un código similar al que se puede ver en el programa `recv_rtp.py` que hemos incluido en el repositorio plantilla de la práctica final. Ese código genera un fichero con los datos recibidos en los paquetes RTP, pero no un fichero MP3 correcto. No hace falta que el fichero generado sea un fichero MP3 correcto,
basta con que se escriban en él los datos recibidos en el `payload` de los
paquetes RTP, en cualquier orden (que es lo que hace `recv_rtp.py`).
Revisa el código de este programa ejemplo con cuidado, los comentarios
deberían aclarar todos sus detalles.

## ServidorRTP (parte básica)

El servidor RTP ha de ejecutarse así:

```shell
$ python3 serverrtp.py <IPReg>:<puertoReg> <fichero>
```

donde:

* `<IPReg>` es la dirección IP de la máquina donde está ServidorRegistrar
* `<puertoReg>` es el puerto UDP donde está escuchando ServidorRegistrar
* `<fichero>` será el fichero con el sonido, que se enviará mediante RTP cuando se establezca una sesión con un cliente

Por ejemplo:

```shell
$ python3 serverrtp.py 127.0.0.1:6001 heyjude.mp3
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverrtp.py <IPReg>:<portReg> <file>
```

Al arrancar, ServidorRTP enviará un `REGISTER` a ServidorRegistrar, para registrarse, utilizando como dirección SIP el nombre del fichero con el audio a enviar (sin extensión) concatenado con `@singasong.net`. Por ejemplo, si el fichero es `heyjude.mp3`, la dirección será `sip:heyjude@signasong.net`. La petición por tanto será como esta:

```
REGISTER sip:heyjude@singasong.net SIP/2.0\r\n\r\n
```

ServidorRegistrar deberá responder con un mensaje OK.

A continuación, ServidorRTP quedará esperando peticiones `INVITE` de los clientes (que le llegarán vía ServidorProxy). Cuando las reciba correctamente, responderá con un OK:

```
SIP/2.0 200 OK\r\n\r\n
```

Y empezará a enviar paquetes RTP al cliente, según los datos que haya podido obtener del documento SDP que viene en el `INVITE` que ha recibido.

Quedará también esperando un paquete `BYE` del cliente (que le llegará vía ServidorProxy). Cuando le llegue, interrumpirá el envío de paquetes RTP a Cliente, y responderá con un OK:

```
SIP/2.0 200 OK\r\n\r\n
```

A partir de ese momento, quedará esperando una nueva petición `INVITE`  de otro cliente.

Para enviar los paquetes RTP al cliente, se puede usar un código similar al que se puede ver en el programa `send_rtp.py` que hemos incluido en el repositorio plantilla de la práctica final. 
Revisa el código de este programa ejemplo con cuidado, los comentarios
deberían aclarar todos sus detalles.


## ServidorRegistrar (parte básica)

El servidor _registrar_ ha de ejecutarse así:

```shell
$ python3 serverreg.py <puerto> <fichero>
```

donde:

* `<puerto>` será el puerto UDP donde recibirá peticiones SIP tanto de clientes como de servidores RTP.
* `<fichero>` será el fichero JSON donde se guardarán los datos de todos los que se registren.

Por ejemplo:

```shell
$ python3 serverreg.py 6001 register.json
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverreg.py <port> <file>
```

Al arrancar, ServidorRegistrar funcionará como _registrar_. Por lo tanto, aceptará la siguiente petición SIP:

* `REGISTER`: Registrará en un diccionario que va a mantener con todas las direcciones registradas. El diccionario tendrá como claves las direcciones SIP que se registren, y como valor para cada clave las tuplas <IP>:<puerto> (dirección IP y puerto de origen del paquete de registro). En esas direcciones será donde los clientes y los ServidorRTPs estarán esperando mensajes SIP. Los datos de este diccionario serán respaldados en el fichero JSON indicado como argumento al ejecutar el programa, con la misma estructura que el diccionario. Este fichero se actualizará cada vez que haya algún cambo en los datos de registro. Cuando arranque el servidor comprobará si existe un fichero con ese nombre, y si es así lo tratará de leer para inicializar el diccionario de registro.


## ServidorProxy (parte básica)

El servidor proxy ha de ejecutarse así:

```shell
$ python3 serverproxy.py <puerto> <fichero>
```

donde:

* `<puerto>` será el puerto UDP donde recibirá peticiones SIP tanto de clientes como de servidores RTP.
* `<fichero>` será el fichero JSON donde se leerán los datos de los que se han registrado con ServidorRegistrar.

Por ejemplo:

```shell
$ python3 serverproxy.py 6004 register.json
```

En caso de no introducir el número de parámetros correctos o de error en los mismos, el programa debería imprimir siempre por pantalla:

```shell
Usage: python3 serverrtp.py <port> <file>
```

Al arrancar, ServidorProxy funcionará como _proxy_. Por lo tanto, aceptará varias peticiones SIP:

* `INVITE`, `ACK`, `BYE`: Cuando los reciba, actuara como _proxy_ reenviándlos al UA SIP correspondiente.

Para saber dónde tiene que reenviar estas peticiones, cuando le haga falta consultará los datos del fichero JSON que se le pasa como argumento, y que será el mismo en el que estará escribiendo ServidorRegistrar. Es importante entender que no basta con leer este fichero al arrancar, porque los datos disponibles en el arranque pueden haber cambiado en el momento en que le hagan falta a RegistrarSIP.

Al actuar como _proxy_ también reenviará a los UAs correspondientes las respuestas SIP (OK o de error) que reciba de otros UAs en respuesta a peticiones SIP que haya cursado con anterioridad.

## Históricos  (parte básica)

Todos los programas (Cliente, ServidorRTP, ServidorRegistrar y ServidorProxy) escribirán en su salida estándar (`stdout`) mensajes históricos (_log_) cuando ocurran ciertos eventos, siempre en el siguiente formato (nótese el punto final):

```
YYYYMMDDHHMMSS Evento.
```

donde `YYYYMMDDHHMMSS` es el tiempo, en ese formato, y `Evento` es:

* Cuando un programa empieza: `Starting...`

* Para cada envío/recepción de una petición o una respuesta SIP: `SIP to/from <ip>:<port> <line>.`, siendo `<ip>` y `<port>` la dirección IP y el puerto de destino o de origen de la petición o la respuesta SIP.

* Cuando un cliente se prepare para recibir paquetes RTP en un puerto: `RTP ready <port>`, siendo `<port>` ese puerto.

* Cuando un ServidorRTP empiece a enviar paquetes RTP a una cierta dirección IP y puerto: `RTP to <ip>:<port>`, siendo `<ip>` y `<port>` esa dirección y ese puerto.

Nótese, por tanto, que cada línea ha de contener la fecha en el formato indicado, luego un espacio en blanco y luego el texto del evento. Por ejemplo:

```
20211128152045 Starting...
20211128153012 SIP to 127.0.0.1:5555: REGISTER sip:heyjude@signasong.net SIP/2.0.
20211128153017 SIP from 127.0.0.1:5555: SIP/2.0 200 OK.
20211128153036 RTP to 127.0.0.1:4444.
```

## Capturas

Han de realizarse las siguientes capturas con Wireshark, que se entregarán con el resto del proyecto en el repositorio correspondiente:

* `capture.libpcap`: Paquetes SIP y RTP intercambiados entre todos los programas en una transferencia de un audio desde un ServerRTP a un cliente, durante 10 segundos. Se realizará lanzando, en este orden (en consolas diferentes) los siguientes programas con los correspondientes argumentos:

```shell
$ python3 serverreg.py 6001
$ python3 serverrpoxy.py 6004
$ python3 serverrtp.py 127.0.0.1:6001 cancion.mp3
$ python3 client.py 127.0.0.1:6001 127.0.0.1:6004 sip:yo@clientes.net sip:cancion@signasong.net 10 received.mp3
```

Además del fichero con la captura, se incluirá también el fichero `received.mp3`.

* `capture2.libpcap`: Paquetes SIP y RTP intercambiados entre todos los programas en una transferencia de un audio desde un ServerRTP a dos clientes, en secuencia, durante 10 segundos con cada uno. Se realizará lanzando, en este orden (en consolas diferentes) los siguientes programas con los correspondientes argumentos:

```shell
$ python3 serverreg.py 6001
$ python3 serverrpoxy.py 6004
$ python3 serverrtp.py 127.0.0.1:6001 cancion.mp3
$ python3 client.py 127.0.0.1:6001 127.0.0.1:6004 sip:yoa@clientes.net sip:cancion@signasong.net 10 received2a.mp3
$ python3 client.py 127.0.0.1:6001 127.0.0.1:6004 sip:yob@clientes.net sip:cancion@signasong.net 10 received2b.mp3
```

Además del fichero con la captura, se incluirán también los ficheros `received2a.mp3` y `received2b.mp3`.


## Peticiones concurrentes (parte adicional)

ServidorRTP acepta peticiones concurrentes de varios clientes

## Configuración XML (parte adicional)

ServidorRTP utiliza un fichero XML para almacenar datos sobre direcciones SIP y nombres de ficheros, de forma que un solo servidor puede servir un conjunto de direcciones, cada una correspondiente con un fichero diferente

## Envío por cliente (parte adicional)

Cliente envía en cada sesión que establezca con ServidorRTP un mensaje de audio. ServidorRTP almacenará todos los ficheros de audio que reciba de los clientes

## Cabecera de tamaño (parte adicional)

Se incluirá en los paquetes SIP la cabecera de tamaño del cuerpo del paquete, cuando haya cuerpo.

## Tiempo de expiracion (parte adicional)

Tiempo de expiración de las direcciones SIP registradas, utilizando la cabecera correspondiente en `REGISTER`. Los clientes se registrarán por el tiempo que se especifique al llamarlos. Los ServidorRTP se registrarán por 62 segundos, y al cabo de 30 segundos renovará el registro enviando un nuevo `REGISTER`, de nuevo con periodo de validez de 62 segundos (así el registro se mantendrá aunque se perdiera un paquete `REGISTER`).

## Gestión de errores (parte adicional)

Gestión de errores, enviando las correspondientes respuestas en lugar de `200 OK`.

## Entrega de la práctica

La práctica se entregará en el repositorio del alumno en el GitLab de la ETSIT, que habrá sido derivado (haciendo "fork", como se indica al comienzo de este enunciado) del repositorio plantilla del proyecto final.

El repositorio deberá incluir los ficheros que había en el repositorio plantilla, más:

* Ficheros con programas principales `serverreg.py`, `serverproxy.py`, `serverrtp.py` y `client.py`.
* Capturas indicadas en el apartado "Capturas", más arriba: `capture.libpcap` y `capture2.libcap`

* Fichero de entrega, `entrega.md`, en formato Markdown, con la siguiente información:

  - Sección sobre la parte básica (`## Parte básica`). Incluirá cualquier comentario relevante sobre la parte básica de la práctica, incluyendo cualquier aspecto que no funcione como se indica en el enunciado.
  - Sección sobre la parte adicional (`## Parte adicional`). Incluirá cualquier comentario relevante sobre la parte adicional de la práctica, incluyendo fundamentalmente el listado de las opciones que se han realizado (una por línea, comenzando por `* `, para que queden en un listado, y con el mismo contenido en esa línea que el nombre del apartado correspondiente paa esa opción). Por ejemplo:

```markdown
* Configuración XML

    Explicación
  
* Cabecera de tamaño

    Explicación
```

  Además de este listado incluirá una explicación (ver el ejemplo anterior) sobre cómo probar esa parte opcional, indicando al menos qué programas hay que lanzar, con qué argumentos, y cómo se puede comprobar que funciona el apartado en cuestion.

  Si se ha realizado cualquier parte adicional que no venga indicada específicamente en el enunciado (por ejemplo, se han implmentado peticiones SIP adicionales, o cabeceras SIP adicionales), puede indicarse en este listado como `Otra:`, seguido de una descripción de la mejora (una entrada `Otra` por cada mejora).

  - Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Es importante comprobar que este documento se ve correctamente desde la interfaz eb de GitLab (eso es, que el marcado Markdown se interpreta correctamente)

## Preguntas y respuestas

### ¿Cómo puedo actualizar mi repositorio si se actualiza el repositorio plantilla de la práctica?

Puede ocurrir que mientras estás trabajando con tu práctica, después de haber hecho tu fork para poder trabajar clonándolo localmente, actualicemos el repositorio plantilla (del que hiciste el fork). Puedes incorporar las modificaciones al repositorio plantilla en el tuyo, de la siguiente manera.

Primero, tienes que añadir, a tu repositorio local (el que clonaste de tu fork) un nuevo `remote`. Los "remote" son los repositorios remotos con los que tu repositorio local puede sincronizarse. Tu repositorio tiene un remote (llamado `origin`) que corresponde con tu repositorio en GitLab (el que creaste como fork del repositorio plantilla). Puedes ver los remotes de tu repositorio:

```shell
git remote -v
```

Verás dos líneas para `origin`, una para `fetch` (recibir datos) y otra para `push` (enviar datos).

Para poder actualizar a partir del repositorio plantilla, vamos a añadir un nuevo remote para él:

```shell
git remote add upstream https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-pfinal-21
```

Si ahora vuelves a ejecutar `git remote -v` verás cuatro líneas, dos para el remote `origin` y dos para el nuevo remote `upstream`.

En este momento es importante comprobar si tienes pendientes modificaciones a ficheros que conoce git (esto es, hay modificaciones que aún no han sido incluidas en ningún commit). Para ello, puedes comprobar que `git status` está limpio, o puedes tratar de realizar un commit que incluya todos los cambios pendintes (si no hay cambios git te lo dirá). Para tratar de haer este commit puedes escribir, en el directorio principal de tu repositorio (atención al `.` final):

```shell
git commit .
```

Ahora que ya tenemos al repositorio plantilla como remote, y estamos seguros de que no hay cambios pendientes, podemos sincronizar con él. Si no has tocado los ficheros que obtuviste del repositorio, git se encargará de gestionar la sincronización completamente. Si los has tocado, puede ser un poco más complicado, pero posible. Por si acaso, antes de realizar los siguientes pasos, copia los ficheros que hayas escrito (tus programas, fundamentalmente) a otro directorio, por si acaso.

La forma más simple de sincronizar los contenidos del repositorio plantilla es la siguiente:

```shell
git fetch upstream
git rebase upstream/master
```

La primera orden bajará la información nueva que pueda haber en el remote `upstream` (el repositorio plantilla de la practica), pero no la incorporara en el directorio. La segunda orden incorporará los commits nuevos que pueda haber en la rama principal de `upstream` en la rama en la que estés trabajando en tu repo local. Para hacerlo, `git rebase` hará comprobaciones, para asegurarse de que esa incorporación es segura, sin perder información. Además, colocará los commits que tengas pendientes en tu rama detrás de los commits que descargue de `upstream/master`.

Si `git rebase` falla, porque no puede asegurar que la incorporación de commits es segura, te lo dirá, dándote varias opciones, ente ella la de abortar. Si es el caso, trata de entender la situación, y en caso de duda, aborta. Probablemente tengas cambios que den conflicto, o tengas cambios si incluir en un commit. Si `git rebase` tiene éxito, ya tienes en tu repositorio local los cambios que se hayan hecho en el repositorio plantilla de la práctica.

Alternativamente a `git rebase` puedes usar `git merge`, que hará algo muy parecido, pero manteniendo la historia de commits de otra forma:

```shell
git fetch upstream
git merge upstream/master
```

Si por algún motivo estás trabajando en una rama distinta de la principal, y quieres incorporar los cambios a la principal, asegúrate que pasas a esa rama principal antes, con `git checkout master`.

### ¿Cómo puedo enviar los paquetes RTP desde ServidorRTP a Cliente?

Utiliza los servicios que proporcion el paquete 'simpletp.py', que está incluido en el repositorio de plantilla de la práctica. Asegúrate de que estás usando la última versión, porque hemos incluido algunas mejoras que simplifican su uso. Puedes ver un ejemplo de cómo usarlo en el fichero 'send_rtp.py', también en el repositorio de plantilla de la práctica (lee sus comentarios, deberían explicar todos los detalles). Su uso se puede resumir en:

* Instancia un objeto de la clase 'simplertp.RTPSender', dándole como parámetros al dirección IP y el puerto UDP a los que enviar los paquetes RTP, y el nombre del fichero MP3 a enviar:

```python
sender = simplertp.RTPSender(ip=IP, port=PORT, file='cancion.mp3', printout=True)
```

* Usa ese objeto para enviar los paquetes. Usa el método que crea un hilo aparte para el envío (`send_threaded`), de forma que continúe la ejecución normal de tu programa principal mientras se está realizando el envío:

```python
sender.send_threaded()
```

### ¿Cómo puedo parar el envío RTP desde ServidorRTP cuando se recibe BYE?

Si has usado el método `send_threaded` para envío de RTP en su propio hilo (thread) que se menciona anteriormente, puedes parar el envío utilizando el método `finish` del objeto 'simplertp.RTPSender' que creaste:

```python
sender.finish()
```

Mira en el programa de ejemplo `send_rtp.py` para ver cómo se usa este método para interrumpir el envío RTP cuando han pasado unos segundos (en tu caso, lo tendrás que hacer cuando se reciba el BYE del cliente, pero la idea es la misma).

**Importante**: Asegúrate de que tienes la última versión de `simplertp.py` (la que está ahora mismo en el repositorio de plantilla de la práctica), porque si no, es posible que no tenga el método `finish` o que este no funcione adecuadamente.

### ¿Cómo puedo recibir los paquetes RTP en Cliente?

Los paquetes RTP que llegan al cliente son paquetes UDP. Por tanto, podemos recogerlos con un objeto `socketserver.UDPServer`, como puedes ver en el ejemplo `recv_rtp.py` que podrás encontrar en el repositorio plantilla de la práctica. Observa que en él hemos usado un manejador (handler), `RTPHandler`, heredado de la clase `socketserver.BaseRequestHandler`, y no de la clase `socketserver.DatagramRequestHandler` como hemos hecho en otras ocasiones. El motivo es que la segunda clase siempre emite un paquete UDP al terminar el método `handle`, con lo que se haya escrito en `self.wfile` (si no se ha escrito nada, enviará un paquete UDP vacio). Y en este caso, queremos que sólo se reciban paquetes RTP, no que se envíe nada como respuesta.

Por lo demás, lee los comentarios de `recv_rtp.py` para hacerte una idea más detallada. Lo principal es que para cada paquete UDP que se reciba, tenemos que recoger la carga (payload) del paquete RTP que va dentro de él (que en general vendrá a partir del byte 12 de los datos del paquete UDP), y escribirla en el fichero de salida donde Cliente tiene que almacenar el audio que llegue.

El fichero generado de esta forma es normalmente un fichero MP3 correcto, que se puede escuchar con cualquier aplicación que nos permita escuchar MP3. Pero hay que tener en cuenta que como los paquetes RTP van sobre UDP, pueden haber sufrido pérdidas y desórdenes, por lo que el fichero resultante podría no oirse como el fichero original, e incluso no oirse en determinadas circunstancias. Estas circunstancias son muy, muy raras si se está probando todo sobre localhost.

### ¿Cómo puedo tener un servidor UDP que escuche en un puerto cualquiera?

Hay varios casos en los que será conveniente crear un `UDPServer` que escuche en cualquier puerto disponible, y luego saber cuál es ese puerto. Esto ocurrirá, por ejemplo en:

* El cliente, para poder crear un receptor de RTP. No hay ningún argumento de línea de comandos para indicar en qué puerto el cliente va a recibir los paquetes RTP, así que habrá que abrir un `UDPServer` que escuche en un puerto libre cualquiera, y luego mediante SDP se indicará a ServidorRTP cuál es ese puerto para que envíe sus paquetes RTP.

* ServidorRTP, para poder crear un receptor de SIP. No hay tampoco ningún argumento de línea de comandos para indicar en qué puerto va a escuchar las peticiones SIP, así que habrá que abrir un `UDPServer` que escuche en un puerto libre cualquiera, y luego se le indicará a ServidorSIP cuando se haga REGISTER.

La forma de crear un `UDPServer` en ambos casos es la misma: indicando, como puerto en la tupla (IP, puerto) que hay que pasarle, el puerto 0. Este "puerto" tiene el significado especial de "elige cualquier puerto libre de la máquina". Igualmente, como dirección IP podemos usar "0.0.0.0" que quiere decir "escucha en todas las interfaces IP que tenga la máquina".

Así, el código será similar al siguiente:

```python
with socketserver.UDPServer(("0.0.0.0", 0), RTPHandler) as serv:
    rtp_port = serv.server_address[1]
```

La línea donde se asigna valor a `rtp_port` nos permite ver cómo podemos obtener el puerto en que ha quedado recibiendo nuestro objeto `UDPServer`.

En el caso de ServidorRTP, podemos usar un código como el siguiente para crear el `UDPServer`, registrarse (enviar la petición REGISTER) usando su socket, y luego lanzar ya el bucle de recepción de peticiones SIP, por si un cliente le envía alguna vía el ServidorSIP. Algo como:

```python
with socketserver.UDPServer(('0.0.0.0', 0), SIPHandler) as serv:
    # Registramos con el socket que usará el servidor SIP
    # register enviará REGISTER a ServidorSIP, y esperará respuesta
    register(serv.socket, client_addr, 3600, server_ip, server_port)
    # Ahora ya podemos activar el bucle para recibir peticiones SIP
    # en el mismo socket
    serv.serve_forever()
```

### ¿Cómo puedo dejar de recibir paquetes RTP en Cliente?

Cuando haya pasado el periodo de recepción que se especificó al invocar `cliente.py`, el programa debe enviar un BYE de SIP, y al recibir el OK correspondiente, debería dejar de recibir paquetes RTP (debería dejar de esperarlos). Para hacerlo, podemos lanzar el bucle de recepción del servidor UDP (`serve_forever`) en un hilo aparte (`serv` es el servidor UDP):

```python
threading.Thread(target=serv.serve_forever).start()
```

De esta manera el programa principal continuará después de lanzar ese hilo, en el que el bucle de recepción empezará a funcionar. Así, el programa principal puede hacer más cosas, como por ejemplo esperar el tiempo que sea necesario, enviar BYE a ServidorRTP (vía ServidorSIP) cuando toque, y terminar el bucle de recepción cuando le llegue el OK de respuesta. Para terminar el bucle de recepción podemos usar este código:

```python
serv.shutdown()
```

Esto terminará el bucle de recepción del servidor UDP, con lo que terminará tambien el hilo que creamos, pues no tiene nada que hacer cuando termine `serve_forever`.

Este código puede verse en el programa de ejemplo `recv_rtp.py`, donde hay un `time.sleep()` para simular con un retardo el tiempo que se estaría esperando a que pase el plazo, el envío de BYE y la posterior recepción de OK.


### ¿Por qué me sale un mensaje de warning al enviar paquetes RTP usando simplertp?

Si cuando envías paquetes RTP usando el paquete `simplertp` que hemos incluido en el repositorio plantilla de la práctica ves mensajes como

```
Warning: Connection refused. Probably there is nothing listening on the other end.
```

lo que muy probablemnte está ocurriendo es justamente lo que dice: que se ha detectado que no hay nada escuchando los mensajes que se envían. El mensaje debería desaparecer cuando te esté funcionando el código de recepción de paquetes RTP. De hecho, si te sale este mensaje, es casi seguro que ese código no está funcionando, o no está escuchando en la dirección y puerto a los que estás enviando los paquetes RTP.

Lee más arriba cómo escribir el código para recibir los paquetes RTP.

### Las aplicaciones, ¿tienen que mostrar sólo los mensajes históricos que se indica?

Las aplicaciones deberían mostrar sólo los mensajes que se indican en el apartado correspondiente (Históricos). En principio, no deberían mostrarse más mensajes en al consola. Es importante que muestren, eso sí, todos los mensajes históricos que se indican en ese apartado.

Si crees que es necesario que se escriba algún mensaje más, documéntalo en el fichero de entrega, en el apartado de comentarios.

### Cuando quiero que un manejador (handler) no termine enviando un mensaje, ¿qué puedo hacer?

La clase `socketserver.DatagramRequestHandler` proporciona un método `handle` y dos objetos que permiten leer el mensaje recibido (`self.rfile`) y  escribir en el mensaje de respuesta (`self.wfile`) cómodamenete. Lo que hayamos escrito en `self.wfile` se enviará automáticamenet al terminar la función `handle`, sin que tengamos que hacer nada al respecto. Si no se hubiera escrito nada en `self.wfile` se envía de todas formas un mensaje vacío de respuesta.

Sin embargo, en este proyecto hay varios casos en lso que no queremos que `handle` termine enviando una respusta a quien envió el mensaje. Esto ocurre por ejemplo en estos casos:

* Cuando ServidorProxy recibe una petición INVITE, ACK o BYE de un cliente. En lugar de responderle, queremos reenviarla al ServidorRTP correspondiente, sin responder nada al cliente.

* Cuando ServidorProxy recibe una respuesta de un ServidorRTP a una petición anterior (`200 OK`, por ejemplo). Queremos reenviarla hacia el cliente, sin responder nada a ServidorRTP.

* Cuando un cliente recibe un paquete RTP de un ServidorRTP. Queremos escribirla en el fichero, sin responder nada a ServidorRTP.

Entre las formas que hay para conseguir esto, recomendamos las dos siguientes:

* Quizás la más cómoda, porque requiere pocos cambios sobre las prácticas anteriores, es seguir usando `socketserver.DatagramRequestHandler`, como hemos hecho en esas prácticas. Para ello, habrá que redefinir el método `finish` de la clase que crearemos hereando de ella. El método `finish` por defecto que proprociona `socketserver.DatagramRequestHandler` es el que se encarga de enviar ese mensaje de respuesta. Al redefirlo en nuestra clase heredada, podemos hacer que sólo se envíe el mensaje cuando nos convenga. Por ejemplo: 

```python
def finish(self):
    if self.result != NO_REPLY:
        # We have to send a response, use the fihish() of the base class
        super().finish()
```

En este código, se supone que tenemos una variable del objeto llamada `self.result`, que cuando vale `NO_REPLY`, queremos que no se responda a quien nos envió el cliente (y por eso el código, en ese caso, no hace nada).

Cuando `self.result` valga algo distinto de `NO_REPLY`, queremos el comportamiento normal de `finish`, y por eso llamamos a la versión de la clase madre (`super().finish()`).

* Otra solución es la que hemos propuesto para recibir paquetes RTP: no heredar de la clase `socketserver.DatagramRequestHandler` sino de la clase `socketserver.BaseRequestHandler`, que también sirve para manejar datagramas, y su `handle` no envía esa respuesta al terminar. Sin embargo, esta clase es más sencilla, y no tiene `self.rfile`, `self.wfile`, ni otras variables que podrían ser interesantes, por lo que recomendamos manejarla con cuidado.

### ServidorProxy: ¿qué peticiones SIP tiene que entender?

ServidorProxy tiene que entender al menos las siguientes peticiones SIP: INVITE, ACK y BYE (porque funciona como un proxy SIP, reenviando estas peticiones que recibirá del cliente hacia ServidorRTP).

Si se implementa alguna otra petición SIP, como por ejemplo TRYING, se puede mencionar como parte adicional.

### ServidorProxy: ¿cómo puedo manejar tanto peticiones de Cliente como respuestas de ServidorRTP?

ServidorProxy aceptará peticiones INVITE, ACK y BYE de Cliente, y se las reenviará a ServidorRTP. En algún momento, éste le enviará sus respuestas (por ejemplo, `200 OK`), y ServidorProxy tendrá que recibirlas, y reenviárselas a Cliente, que estará esperando por ellas. ¿Cómo se puede hacer esto?

Se recomienda tener un solo servidor UDP que en su método `handle` trate de discriminar si le está llegando una petición o una respuesta, y actúe de acuerdo con ello. Para simplificar la tarea se pueden seguir el siguiente detalle:

* En la clase manejadora (la que incluye `handle` incluimos un diccionario como variable de la clase (por ejemplo, lo llamamos `waiting`). Este diccionario tendrá como claves las direcciones (IP y puerto) de destino de cada petición que se haya repetido hacia un ServerRTP, y como dato asociado a cada clave, la dirección (IP y puerto) del cliente que envió esa petición.

* Como un par (IP, puerto) no puede ser directamente la clave de un diccionario, obtendremos una cadena (string) de ella usando el método `str`.

* Así, por ejemplo, si el cliente ('127.0.0.1', 45678) ha enviado una petición para un ServidorRTP que escucha en ('127.0.0.1', 23456), el diccionario `waiting` tendrá una entrada de clave str(('127.0.0.1', 23456)) y de valor ('127.0.0.1', 45678):

```python
waiting = {...
           str(('127.0.0.1', 23456)): ('127.0.0.1', 45678)}
```

* De esta manera, cuando llegue una respuesta, bastará con mirar su dirección origen, buscarla en el diccionario `waiting`, y el valor que tenga esa clave será la dirección donde hay que enviar esa respuesta. De paso, se podrá quitar esa clave de `waiting`, ya que se ha recibido la respuesta correspondiente.

* Para saber la dirección de origen de una petición o de una respuesta, en `handle` se puede consultar `self.client_address`.

* La dirección de destino de una petición (la dirección donde está el ServidorRTP a la que va destinada) es aquella con que se haya registrado.

### ServidorProxy: ¿cómo puedo hacer para reenviar peticiones y respuestas?

Cuando se recibe una petición o una respuesta, una vez sabemos a dónde tenemos que enviarlas (vía el diccionario `waiting` para las respuestas, o vía el diccionario de registros para peticiones), lo más sencillo es coger el mensaje que nos ha llegado, tal cual, y reenviarlo. Por ejemplo:

```python
self.socket.sendto(self.packet, dest_addr)
```

En este caso, estamos usando `self.packet`, que en los manejadores (handlers) de paquetes datagramas contiene el paquete recibido, tal cual, para reenviarlo a la dirección 'dest_addr' usando el socket que tiene el propio manejador (`self.socket`).

### Cliente: ¿en que puerto voy a escuchar las respuestas SIP que me lleguen de ServidorRegistrar o ServidorProxy?

El cliente, cuando se lanza en la línea de comandos, no tiene ningún argumento para indicar en qué puerto va a escuchar. Por lo tanto, puede escuchar en cualquier puerto UDP que esté libre en el momento de ser lanzado. Cuando envíe el mensaje REGISTER a ServidorRegistrar, lo enviará desde ese puerto, y así ServidorRegistrar sabrá qué dirección IP y puerto UDP tiene que registrar para ese cliente.

Por eso es importante que el cliente realice las siguientes acciones:

* Abra un socket UDP con recepción en cualquier puerto.

* Utilice ese socket para enviar la petición REGISTER a ServidorRegistrar.

* Quede escuchando en ese socket (y por tanto en el puerto con el que se ha regitrado) la respuesta de ServidorRegistrar.

* Envíe cualquier petición SIP para ServidorRTP (que siempre enviará a ServidorProxy) usando ese socket, y quede esperando las respuestas también en ese socket.

Para conseguir un socket en el que enviar y recibir estos mensajes, y que escuche en cualquier puerto, no hay que hacer nada especial. Basta con el código normal que hemos usado para obtener un socket:

```python
import socket
...
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
```

### ServidorRTP: en la parte adicional "concurrentes", ¿cómo puedo saber qué cliente me está enviando un BYE, para terminar el envío correspondiente?

Si se implementa la parte adicional "peticiones concurrentes", puede ocurrir que un ServidorRTP reciba INVITE de dos clientes distintos, y empiece a enviar datos RTP a cada uno de ellos. ¿Cómo se puede saber, cuando llega una petición BYE a cuál de los dos clientes corresponde?

Hay varias formas en que se puede implementar esto, pero quizás la más sencilla pase por implmentar la cabecera SIP 'From' en las peticiones del cliente, con el siguiente formato:

```
From: <sip:yo@dir>;tag=3rffre34544nnre445
```

Esta es una cabecera correcta de SIP, que indica de dónde viene la petición. La dirección SIP es la del cliente, y la etiqueta (`tag`) es una cadena de texto aleatoria (que en este caso incluye dígitos y letras minúsculas), que es la misma sólo para la misma sesión (la secuencia INVITE, ACK y BYE para establecer y liberar una conversación).

Usando esta cabecera, ServidorProxy puede anotar en un diccionario, que podrá ser una variable de la clase de su clase manejadora ("handler"), a qué `From` corresponde cada emisor de RTP que inicie. Para ello puede usar como clave el contenido del campo `From` de la petición que lo inició, y como dato el objeto emisor RTP de `simplertp`).

Así, cuando llegue un BYE, tendrá tambié un campo `From`. Usando su valor en ese diccionario, podrá saber el objeto emisor RTP del que tiene que llamar `finish` para terminar la emisión.

Podemos generar un tag aleatorio fácilmente con un método `randomstr` como este:

```python
import secrets
import string
...
def randomstr():
   letters = string.ascii_lowercase
   return ''.join(secrets.choice(letters) for i in range(20))
```

# Protocolos para la transmisión de audio y video en Internet
# Práctica 2. Python avanzado

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta 0.77 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de Python3 (las de la primera práctica).
* Nociones de git (las de la práctica de git)

**Tiempo estimado:** 6 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-p2

<!--
**Práctica resuelta:** https://gitlab.etsit.urjc.es/ptavi/2021-2022-resueltas/ptavi-p2-21-resuelta
-->

**Fecha de entrega parte individual:** 13 de octubre de 2022, 23:59 (hasta ejercicio 10, incluido)

**Fecha de entrega parte interoperación:** 20 de octubre de 2021, 23:59 (ejercicio 11)

## Introducción

La programación orientada a objetos en un paradigma de programación muy
utilizado en la actualidad y que conviene conocer para la realización de las
prácticas de la asignatura. En esta práctica se pretende explorar los conceptos
más importantes de este paradigma, implementando funcionalidad en Python.

## Objetivos de la práctica

* Conocer y practicar la programación orientada a objetos (con conceptos como clase,
  objeto, herencia, instanciacion).
* Usar varios modulos Python, importando funcionalidad creada en otros modulos.
* Conocer y seguir la guía de estilo de programacion recomendada para Python (ver PEP8).
* Utilizar el sistema de control de versiones git en GitLab.

## Ejercicio 1

Comprueba que puedes entrar en tu cuenta del [GitLab de la ETSIT](https://gitlab.etsit.urjc.es).

## Ejercicio 2

Con el navegador, dirígete al repositorio plantilla de esta práctica (ver al comienzo de este enunciado)
y realiza un "fork" del mismo en tu cuenta, de manera que consigas tener una
copia del repositorio en tu cuenta. Clona en tu ordenador
local el repositorio que acabas de crear, para poder editar los archivos localmente.
Trabaja a partir de ahora en ese repositorio, sincronizando
los cambios (commits) que vayas efectuando según los ejercicios que se
detallan a continuación. Recuerda subir estos cambios a tu repositorio en
GitHub (normalmente, usando `git push`) para que queden reflejados allí.

## Ejercicio 3

Investiga el archivo `compute.py`, en este repositorio.
Comprueba que en él se implementa un programa para calcular potencias y logaritmos.
El programa tiene las siguientes características:

* Se invoca de la siguiente manera desde el terminal (shell):

```shell
python3 compute.py <operation> <num> [<num2>]
```

donde `<operation>` podrá ser `poswer` o `log`. Por ejemplo:

```shell
python3 compute.py power 2 3
8.0
```

* Para tomar los operandos de los argumentos que se pasan al programa cuando se
ejecuta, se usa el módulo sys (`import sys`).
La lista `sys.argv` tiene los argumentos que se especificaron al ejecutar el programa.

* El programa comprueba que:
  * el número de parámetros es correcto (al menos dos, que teniendo en cuenta que el primer elemnto de `sys.argv` es el nombre del programa, quiere decir que hay tres elementos en `sys.argv`)
  * los parámetros correspondientes a números son numéricos (pueden convertirse a `float`), y si no levanta una excepción que mostrará un error.
  * la operación es `poswer` o `log`, y si no es así se muestra también  un mensaje de error.

* Tiene dos funciones (métodos): `poswer` para la potencia y `log` para el logaritmo.

* Muestra el resultado final en pantalla.

## Ejercicio 4

Copia el fichero 'compute.py' en tu clon local de tu repositorio para la práctica.
Añádelo a los ficheros que conoce git ( git add`), y no olvides hacer un commit
para registrar el cambio.

Ejecuta, desde esa copia en el clon local de tu repositorio para la práctica,
el programa `compùte.py` en el terminal y desde PyCharm, con varios argumentos, incluyendo algunos que fuercen que se muestren los mensajes de error.
Pruébalo también en el depurador de PyCharm, estudiando cuánto valen las variables
que utliza según el prograna se va ejecutando.

Para ejecutarlo en PyCharm, tendrás que editar la
[configuración de ejecución del programa](https://www.jetbrains.com/help/pycharm/run-debug-configuration.html)
(puedes acceder a ella en el menú desplegable junto al icono de ejecución),
e indicar en ella los parámetros de ejecución como los escribirías en el terminal.

## Ejercicio 5

Crea en el archivo `computeoo.py` un programa Python que implemente la
misma funcionalidad (y se ejecute de la misma manera) que `compute.py`,
pero utilizando construcciones de orientación a objetos.

Para ello, crea una clase llamada `Compute`
(las mayúsculas son importantes), que tenga los métodos (funciones) `power` y `log`
(igual que antes, devolverán el resultado, sin imprimir nada en pantalla).
El método `__init__` de esta clase inicializará una variable `self.default` (valor por defecto del exponente o la base) a 2, y este valor se utlizará en `power` y `log` para el valor por defecto del parámetro correspondiente.

El programa principal de `computeoo.py` aceptará los mismos parámetros
que `compute.py`, y funcionará de la misma manera. Pero ahora, en lugar de llamar
directamente a las funciones `power` y `log`, deberá instanciar un objeto
de la clase `Compute`, y luego llamar a los métocos `power` y `log` de este objeto.

Para entender cómo hacer este ejercicio, puedes estudiar primero el fichero
`ejemplo-oo.py`, que proporciona una clase Persona, otra clase Alumno que hereda
de ella, y en el programa principal instancia objetos de ambas clases, e invoca
métodos de ambos objetos. Te recomendamos también que estudies el
[capítulo "Classes & Iterators" de "Dive into Python3"](https://diveintopython3.net/iterators.html).
Por ahora solo te hará falta la parte donde explica cómo se definen las clases, cómo se instancian,
y cómo funcionan las variables de la instancia.

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `computeoo.py` a los ficheros conocidos por git.

## Ejercicio 6

Crea el archivo `computeoochild.py`, en el que escribirás una nueva clase,
`ComputeChild`, que heradará de `Compute` (de `computeoo.py`), y añadirá dos métodos más:

* `set_def`, que hará que si una operación de potencia o logaritmo no especifica la base o exponente, se usa el valor por defecto que se ha especificado (`default`). Si no se ha especificado, este valor será 2. Además, `set_def` no aceptará una base igual o menor a 0: si se intenta poner, levantará (usando `raise`) una
excepción `ValueError` con el mismo mensaje.
* `get_def` que devolverá el valor por defecto que se está usando.

El programa principal hará lo mismo que el de `computeoo.py`.

Para hacer esto, no olvides, en `computechild.py` importar el módulo `computeoo.py`:

```py
import computeoo
```

Cuando tengas que referirte a `Compute` de `computeoo.py`, tendrás que hacerlo con la
notacion punto: `computeoo.Compute`.

Para entender mejor cómo gestionar las excepciones, explora el [apartado "Exceptions"
en "Dive into Python3"](https://diveintopython3.net/your-first-python-program.html#exceptions).

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente el fichero `computeoochild.py` a los ficheros conocidos por git.

## Ejercicio 7

Crea el archivo `cimputecount.py`, que tendrá una clase `Compute` que no heredará de ninguna otra, e implementará toda la funcionalidad de `ComputeChild` (funciones `power`, `log`, `set_def` y `get_def`).
Además, implementará un variable de instancia (`count`) que almacenará el número de veces que
se ha llamado a cualquiera de las funciones de la clase.

Este archivo no tendrá programa principal.

Para probar el módulo `computecount.py`, utiliza los tests que puedes encontrar en el
archivo `tests/test_computecount.py`. Ejecútalo desde PyCharm, pero también desde el terminal, poniéndote en el directorio principal del repositorio, y escribiendo:

```shell
python3 -m unittest tests/test_computecount.py
```

Aprovecha para leer sobre tests en [Testing Your Code](https://docs.python-guide.org/writing/tests/),
de "The Hitchhiker's Guide to Python" (sobre todo la parte sobre `unittest`) y
la documentación de [`unittest`](https://docs.python.org/3/library/unittest.html).

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente  el fichero `computecount.py` a los ficheros conocidos por git.


## Ejercicio 8

Crea el archivo `computecsv.py` que será llamado con un nombre de fichero como argumento:

```
python3 computecsv.py fichero
```

El fichero será un fichero de texto, con dos o más líneas.
Cada línea estará en [formato CSV (comma-separated-value)](https://en.wikipedia.org/wiki/Comma-separated_values).
La primera línea será para especificar la base, y tendrá la forma:

```
2
```

para indicar que el valor por defecto de la clase `Compute` será dos.

El resto de las líneas tendrán la forma:

```
<operation>,<num>[,<mum2>]
```

Si `num2` no se especifica, se utilizará el valor por defecto indicado.

Para la primera línea, `computecsv.py`leerá el valor por defecto, y mostrará en pantalla el mensaje `Default: <def>` (siendo `<def>` el valor especificado).

Para cada una de las demás líneas, `computecsv.py`  deberá tomar la operación que se especifique en cada línea, y realizarla, imprimiendo en pantalla el resultado final de ejecutar cada línea.

Al terminar, el programa escribirá en una línea `Operations: <num>`, donde `<num>` es el número de operaciones (potencias o logaritmos) realizados.

Así, para las siguientes líneas:

```
3
power,2
power,2,4
log,27
log,16,4
```

el resultado que se imprimirá será:

```
Default: 3
8.0
16.0
3.0
2.0
Operations: 4
```

Si alguna de las líneas no tiene el formato adecuado, el programa escribirá una línea en la que ponga `Bad format` y continuará procesando la siguiente línea. Si esto pasa en la primera línea, se pondrá el valor por defecto 2.

Si el valor por defecto especificado en la primera línea no es mayor que cero, deberá capturarse la excepción, escribir en pantalla el siguiebnte mensaje, y terminar:

```
Error: default value must be greater than zero
```


El fichero `computecsv.py` no implementará los métodos ni las clases para realizar las operaciones, sino que hará uso de la funcionalidad implementada en `computecount.py`, esto es,
de la funcionalidad implementada por la clase `Calc` de `computecount.py`.

El fichero `computecsv.py` proporcionará la funcionalidad indicada mediante una función, `process_csv`, que aceptará como argumento un nombre de archivo, y escribirá en  pantalla el resultado de operar lo especificado en las líneas de ese fichero, como se indica más arriba.

Para leer el fichero, utiliza el formato que hace uso de al la sentencia `with` de Python.
Puedes leer sobre esta sentencia en ["Closing files automatically" de "Dive into Python3](https://diveintopython3.net/files.html#with).

Para extraer la operación y los operandos de cada línea, considera usar la función [`split`](https://docs.python.org/3/library/stdtypes.html#str.split),
que se puede usar con cualquier string. Para eliminar el carácter fin de línea que habrá en cada
línea del fichero, si te hace falta, considera usar la función [`rstrip`](https://docs.python.org/3/library/stdtypes.html#str.rstrip),
que se puede usar también con cualquier string.

Aségurate de que tu programa funciona al menos con el fichero `operations.csv`, produciendo el siguiente resultado:

```
Default: 3.0
8.0
16.0
3.0
2.0
Bad format
Bad format
8.0
Operations: 5
```

Para probar el módulo `computecsv.py`, utiliza también los tests que puedes encontrar en el archivo `tests/test_computecsv.py`. Ejecútalo desde PyCharm, pero también desde el terminal, poniéndote en el directorio principal del repositorio, y escribiendo:

```shell
python3 -m unittest tests/test_computecsv.py
```

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente el fichero `computecount.py` a los ficheros conocidos por git.

## Ejercicio 9

Aunque el intérprete de Python admite ciertas libertades a la hora de
programar, los programadores de Python con la finalidad de mejorar
principalmente la legibilidad del código recomiendan seguir una guía
de estilo. Esta guía de estilo se encuentra en el [Python Enhancement
Proposal 8 (PEP 8)](https://www.python.org/dev/peps/pep-0008/), y contiene instrucciones sobre cómo situar los
espacios en blanco, cómo nombrar las variables, etc. Puedes leer también una
[explicación en español](https://ellibrodepython.com/python-pep8).

Hay programas para comprobar que un programa sigue correctamente PEP8, que además
te explican qué errores tienes si encuentran algunos. Para Python3 recomendamos
utilizar [pycodestyle](https://pycodestyle.pycqa.org)
(en Ubuntu, disponible si instalas el paquete `pycodestyle`), que puedes
aplicar a tus programas hasta que no detecte ningún error.
También puedes usar la [funcionalidad que proprociona PyCharm](https://www.jetbrains.com/help/pycharm/tutorial-code-quality-assistance-tips-and-tricks.html),
que tambien detecta los errores PEP8 si está adecuadamente configurado,
y te puede ayudar a reformatear el código para resolver esos errores.

Recuerda hacer un commit cuando termines el ejercicio (por lo menos).

No olvides sincronizar tus ficheros al repositorio de entrega (tu repositorio en
el GitLab de la ETSIT) para que podamos corregirlos (`git push`), y cuando hayas
terminado todo, comprobar que tu repositirio es público.

# Ejercicio 10

Asegúrate, comprobando enel GitLab de la ETSIT, que todos tus cambios están en tu repositorio de entrega de la práctica.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio de entrega, creado de la forma que 
hemos indicado (como fork del repositorio plantilla que os proporcionamos).
Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:
* Se valorará que haya realizado al menos haya ocho commits sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorará que los programas se invoquen exactamente según se especifica, y que muestren
mensajes y errores correctamente según se indica en el enunciado de la práctica.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.

## Ejercicio 11 (segundo periodo)

Para realizar esta práctica, tendrás que comenzar creando un nuevo grupo en el GitLab de la ETSIT. Los grupos son una especie de usuarios virtuales, que permiten que varios usuarios colaboren en un proyecto común. Por ejemplo, [ptavi](https://gitlab.etsit.urjc.es/ptavi) es uno de estos grupos. En este caso, los vamos a utilizar para poder hacer un segundo fork con origen en el repositorio plantilla de la práctica, como verás más adelante.

Para hacer un grupo, puedes seguir las [instrucciones en el manual de GitLab](https://docs.gitlab.com/ee/user/group/#create-a-group). Como nombre del grupo utiliza `forks-<user>`, siendo `<user>` el nombre de usuario que estás usando en el GitLab de la ETSIT. Así, si tu nombre fuera `jgb`, tu grupo sería `forks-jgb`.

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del [repositorio plantilla de esta práctica](https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p2) uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la ETSIT de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `ptavi-p2-<alumno>`.

Crea un clon local de este nuevo repositorio (usando `git clone`), y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Llama a esta rama `nueva`. Esto lo podrás hacer con el siguiente comando (que crea la rama `nueva`  y te deja en ella:

```shell
git checkout -b nueva
```

Podrás ver en qué rama estás con el comando `git branch`. Si te hace falta, podrás cambiar de nuevo a la rama principal con `git checkout master`, y volver a la rama `nueva` con `git checkout nueva` (ojo: si has hecho cambios, tendrás que hacer primero un commit con ellos, para poder cambiar de rama sin perder datos).

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Por lo tanto, podrás usar su módulo `computecount.py`. Crea uno nuevo, `compute<nombre>.py` (`<nombre>` será tu identificador en el laboratorio).
En este nuevo módulo crearás una nueva clase `computeCalls` que heredará de
`computecount.Compute`. Esta nueva clase incluirá un nuevo método, `calls`, que devolverá el número de veces que se ha llamado a funciones de `ComputeCalls` (`power`, `log`). Recuerda que ese valor lo tendrás en la variable de instancia `count`, así que construir este método será fácil.

Crea también un test en el directorio tests (`tests/test_compute<nombre>.py`) que compruebe que si se llama tres veces a las funciones de `ComputeCalls` el valor que devuelve este nuevo método `calls` es 3.

Cuando termines, crea un commit (usando `git add` y `git commit`, o desde PyCharm) que inclya estos cambios. Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes suar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).

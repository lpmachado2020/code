#!/usr/bin/python3
# -*- coding: utf-8 -*-

def mayor(primero, segundo):
    if primero > segundo:
        return primero
    else:
        return segundo

lista: list = [5, 3, 6, 8]
mayor_primeros = mayor(lista[0], lista[1])
print(mayor_primeros)
mayor_segundos = mayor(lista[2], lista[3])
print(mayor_segundos)
print(mayor(mayor_primeros, mayor_segundos))
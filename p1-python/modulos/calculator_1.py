#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Very simple calculator"""

import calc

if __name__ == "__main__":
    ops = {
        '+': calc.add,
        '-': calc.sub,
        '*': calc.mul,
        '/': calc.div
    }
    first = float(input("Please enter an integer/float (first number): "))
    second = float(input("Please enter an integer/float (second number): "))
    op = input("Please enter a sign (+,-,*,/): ")
    result = ops[op](first,second)
    print(f"The result of {first} {op} {second} is {result}")
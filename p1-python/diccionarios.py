#!/usr/bin/python3

paises = {'de': 'Alemania', 'fr': 'Francia', 'es': 'España'}
print(paises); print(paises["fr"])

extensiones = {}
extensiones['py'] = 'python'
extensiones['txt'] = 'texto plano'
extensiones['doc'] = 'Word'

print("Paises:")
for pais in paises:
   print(pais, paises[pais])

print("Otra vez:")
for pais, nombre in paises.items():
   print(pais, nombre)

del paises['fr']
print(len(paises))
paises.clear()

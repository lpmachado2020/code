# Protocolos para la transmisión de audio y video en Internet
# Práctica 3. XML y JSON

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta 0.77 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de Python3 (primera práctica)
* Nociones de orientación a objetos en Python3 (segunda práctica)
* Nociones de XML y SMIL (presentadas en clase de teoría)

**Tiempo estimado:** 10 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p3-21

**Práctica resuelta:** https://gitlab.etsit.urjc.es/ptavi/2021-2022-resueltas/ptavi-p3-21-resuelta

**Fecha de entrega parte individual:** 21 de octubre de 2021, 23:59 (hasta ejercicio 8, incluido)

**Fecha de entrega parte interoperación:** 24 de octubre de 2021, 23:59 (ejercicio 9)


## Introducción

Python ofrece una serie de bibliotecas para manipular ficheros en XML (como SMIL). En esta práctica, veremos cómo utilizar la biblioteca SAX.

## Objetivos de la práctica

* Profundizar en el uso de SMIL, XML y JSON.
* Aprender a utilizar la biblioteca SAX para el manejo de XML, en particular con Python.
* Utilizar el sistema de control de versiones git en GitLab.

## Ejercicio 1

Con el navegador, dirígete al repositorio [ptavi-p3-21](https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p3-21) y realiza un fork ([instrucciones sobre cómo hacer un fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)), de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona en tu ordenador local el repositorio que acabas de crear a local para poder editar los archivos (usando `git clone`).

Trabaja a partir de ahora en la práctica, registrando los ficheros que vayas añadiendo (usando `git add`) y los cambios que vayas haciendo (usando `git commit`) al hacer los próximos ejercicios. Recuerda que al final del todo tendrás que subir (empujar) estos cambios al repositorio en GitLab (usando `git push`).

## Ejercicio 2

Inspecciona el fichero `chistes.py`, en este mismo directorio. Verás que el fichero consta de tres partes:

* Importación de un método y una clase del módulo `xml.sax`. Nótese cómo la importación es ligeramente diferente a lo que hemos usado hasta ahora. Mediante esta forma incluimos el espacio de nombres de los módulos que importamos, por lo que no hace falta poner el nombre del módulo al llamar en nuestro programa a las clases, métodos y variables de esos módulos. 

* La clase `ChistesHandler`, que hereda de la clase ContentHandler. Los métodos de esta clase son eventos que el analizador (`parser`) lanza cuando se encuentre una etiqueta de inicio (función `startElement`), una etiqueta de final (función `endElement`) o entre una etiqueta de inicio y final (función `characters`). En este último caso, dependiendo de qué bandera (flag) `inPregunta` o `inRespuesta` sea cierta (`True`), se irá almacenando el contenido en la variable correspondiente (`self.pregunta` o `self.respuesta`).

* Las instrucciones de ejecución en el programa principal (función `main`):
  * creación del `parser`
  * instanciación de la clase `ChistesHandler`
  * configuración del `parser` para que use `ChistesHandler` como manejador
  * análisis (parsing) del fichero `chistes.xml` (que también encontrarás en el repositorio).

El manejador de este programa (el objeto `cHandler`, de la clase `ChistesHandler`) guarda el contenido de los elementos en una variable de instancia y, a continaución, lo borra, para dejarla preparada para cuando encuentre el siguiente elemento de interés.

Modifica este programa para que imprima por pantalla las preguntas, las respuestas y la calificación de cada uno de los chistes, según se van encontrando en el archivo `chistes.xml`. Asegúrate de que las instrucciones del programa principal están en una funcion (llamada `main`), pues así lo espera el test.

Puedes probar el programa ejecutando el test `test_chistes.py`:

```shell
python3 -m unittest tests/test_chistes.py
```

Incluye el programa, ya modificado, en el repositorio de entrega (`git add`), y registra el cambio (`git commit`).

## Ejercicio 3

Habrás observado que en el fichero XML `chistes.xml`, en algunos casos la respuesta está antes de la pregunta. Crea un nuevo programa, `chistes2.py`, que haga lo mismo que se especifica en el ejercicio anterior, pero escribiendo cada chiste siempre en este orden, y de esta manera:

```
Chiste número: <num> (<califiacion>)
Pregunta: <pregunta>
Resupuesta: <respuesta>
```

Por ejemplo:

```
Chiste número: 3 (malillo)
Pregunta: ¿En que se parece un elefante a una cama?
Respuesta: En que el elefante es un paquidermo y la cama es 'pakiduermas'
```

Además, tras cada chiste, deja una línea en blanco, para que se diferencien mejor.

Asegúrate de que en el programa, el porgrama principal está también en una función `main`, igual que el anterior.

Puedes probar el programa ejecutando el test `test_chistes2.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 4

Crea un fichero `smil.py`, con una clase llamada `SMILHandler` que herede de la clase `ContentHandler`. Las etiquetas SMIL que deberá reconocer nuestra clase son las siguientes (se enumeran, junto con las etiquetas, los atributos que se han de tenerse también en cuenta):
  
* `root-layout` (width, height, background-color)
* `region` (id, top, bottom, left, right)
* `img` (src, region, begin, end, dur)
* `audio` (src, begin, dur)
* `textstream` (src, region, fill)

La clase `SMILHandler` deberá tener, además, una función (método) llamada `get_tags` que devolverá una lista con las etiquetas encontradas, sus atributos y el contenido de los atributos. Piensa bien cómo ha de ser esta lista (nótese que el orden de las etiquetas es importante y se ha de preservar, pero no es necesario que sea así con los atributos). Puedes utilizar para probar el fichero 'karaoke.smil' que encontrarás en el repositorio plantilla de la práctica, aunque tu código debería funcionar con cualquier SMIL con las etiquetas indicadas más arriba.

Puedes probar el programa ejecutando el test `test_smil.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).


## Ejercicio 5

Crea un programa principal, en un archivo llamado `karaoke.py`, que haga uso de la clase `SMILHandler`. Su programa principal estará en una función `main`, igual que los anteriores. Este programa deberá:

* Leer un fichero SMIL que se pase como argumento en la línea de comandos. En caso de que no se especifique un fichero, mostrará por pantalla:

```
Usage: python3 karaoke.py <file> 
```

Para realizar pruebas, se puede utilizar el fichero `karaoke.smil`. Nota que `<file>` es un argumento que se pasa al programa, y que podrá ser cualquier fichero que tenga un documento en formato SMIL.

* Mostrar por pantalla un listado ordenado de las etiquetas y de los pares atributo-valor (para aquéllos que tengan un valor asignado), cada uno en una línea, separados por tabuladores y sin espacios antes y después del signo igual, tal y como se muestra a continuación.


```
Elemento1\tAtributo11="Valor11"\tAtributo12="Valor12"\t...\n
Elemento2\tAtributo21="Valor21"\tAtributo22="Valor22"\t...\n
...
```

Atención a los tabuladores (`\t`) y los saltos de línea (`\n`). Puedes utilizar las cadenas f (f-string) de Python 3. Nota que, como se comentaba antes, los elementos han de seguir el orden del SMIL, mientras que los atributos dentro de los elementos no hace falta que guarden el orden. Para esto, escribirás una funcion `to_string`, que recibirá como argumento el valor que devuelve `SMILHandler.get_tags`, y devolverá el string con el texto descrito.

Se valorará que la salida del programa siga al pie de la letra lo indicado. En la versión final a entregar, por tanto, no imprimas mensajes de trazas.

Ejemplo de salida correcta:

``` 
root-layout\twidth="248"\theight="300"\tbackground-color="blue"\n
region\tid="a"\ttop="20"\tleft="64"\n
```

## Ejercicio 6

Añade al programa `karaoke.py` la funcionalidad de crear una salida en formato JSON, en lugar de la salida anterior, cuando se especifique en la línea de comandos el argumento `--json`. Este formato será de la forma:

```
[
  {"name": "Elemento1",
   "attrs": {
    "Atributo11": "Valor11",
    "Atributo12": "Valor12",
    ...
    }
  },
  {"name": "Elemento2",
   "attrs": {
    "Atributo21": "Valor21",
    "Atributo22": "Valor22",
    ...
    }
  }
  ...
]
```

Para esto, escribirás una funcion `to_json`, que recibirá como argumento el valor que devuelve `SMILHandler.get_tags`, y devolverá el string con el documento JSON descrito.

Se valorará que la salida del programa siga al pie de la letra lo indicado. En la versión final a entregar, por tanto, no imprimas mensajes de trazas.

Puedes probar el programa ejecutando el test `test_karaoke.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 7

Crea otro programa `download.py` que use también `smil.py`, también con su programa principal en una función `main`. Este programa aceptará un argumento en la linea de comandos, el nombre del archivo SMIL a procesar, y descargará en local el contenido multimedia remoto referenciado en ese archivo SMIL. De esta manera, si el atributo `src` de un elemento del archivo SMIL tiene como valor un elemento en remoto (o sea, algo que empiece por `http://` o `https://`), se deberá descargar ese elemento en local. Para descargarnos por ejemplo `http://gsyc.es/logo.gif`, utilizaremos la función [`urlretrieve`](https://docs.python.org/3/library/urllib.request.html#urllib.request.urlretrieve) de la biblioteca `urllib.request` de Python3.

Cada fichero que descargue lo descargará con el nombre `fichero-<n>.<ext>`, donde `fichero` será siempre el principio del nombre del fichero, `<n>` (empezando por 0) será el número de fichero que se descarga, en el orden en el que están en el fichero SMIL, y `<ext>` la extensión de la url que se descargó. Para identificar la extensión, se considerará suficiente partir la url usando el carácter punto como divisor (usando `split`) y quedarse con el último elmento de la lista resultante (quien quiera realizar una división más completa, puede usar `urlparse` y `splitext`).

Por ejemplo, si las urls son:

```
https://un.servidor.com/fichero.jpeg
http://otro.servidor.org/direct.orio/fich.2.mp3
```

los ficheros en que se crearán serán `fichero-0.jpeg` y `fichero-1.mp3`.

Puedes probar el programa ejecutando el test `test_download.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).


## Ejercicio 8

Crea un nuevo programa, `karaobjeto.py`, que implemente la funcionalidad descrita en los ejercicios 5 al 7 con una nueva clase, `Karaoke`, que utilizará la clase construida en el ejercicio 4 (`SMILHandler`), y que se comportará de la siguiente forma:

* Aceptará como argumentos un nombre de fichero (obligatorio), que puede estar precedido de la cadena `--json` (opcional). Si no se aporta nombre de fichero, o aparece anted de él una cadena que no sea `--json`, se mostrará el siguiente mensaje

```
Usage: python3 karaobjecto.py [--json] <file> 
```

* La clase `Karaoke` deberá tener los siguientes métodos:

  * Inicializador (`__init__`): se le pasará como parámetro el fichero fuente SMIL que el usuario introduce por vía de comandos.  El constructor analizará el fichero SMIL y obtendrá las etiquetas (y sus atributos), utilizando un objecto de tipo `SMILHandler` (del módulo `smil.py`), y su función `get_tags`. Almacenará estas etiquetas (y sus atributos) en una variable de la instancia llamada `tags` (`self.tags`)

  * `__str__`: función que devolverá un string listo para ser imprimido como se hacía en el ejercicio 5, a partir de la variable de instancia `tags`.

  * `to_json`: producirá un string con un documento JSON, tal y como se hacía en el ejercicio 6.

  * `download`, que incluya la funcionalidad para descargar los recursos remotos, según se hace en el ejercicio 7.

* El programa principal estará implementado en una función `main`, como hemos hecho con anterioridad, y realizará lo siguiente:

  * Analizará los argumentos de la línea de comandos, y creará un objeto de una nueva clase, `Karaoke`. A continuación llamará a los métodos (funciones) de este objeto que necesite.

  * Si no se ha especificado el argumento `--json` imprimirá el objeto creado, usando `print` (se ha de tener en cuenta que para imprimir un objeto, `print` llama al método `__str__` del objeto para convertirlo a string).
  
  * Si se ha especificado el argumento `--json` imprimirá el contenido en formato JSON, usando la función `to_json` del objeto.

  * En cualquiera de los dos casos, se realizará la descarga de los elementos del fichero llamando a la función `download` del objeto.

Nótese que el programa sólo deberá imprimir por pantalla las salidas que se indican. El resto del programa, al entregar la práctica, no ha de contener trazas (esto es, no ha de tener más llamadas a `prints`, por ejemplo).

Puedes probar el programa ejecutando el test `test_karaobjeto.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:
* Se valorará que haya realizado al menos haya seis commits, correspondientes más o menos con los ejercicios pedidos, sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorar ́a que los programas se invoquen exactamente según se especifica, y que muestren  mensajes y errores correctamente según se indica en el enunciado de la práctica.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Para muchos de los apartados del programa, se proporciona un test. Puedes ejecutarlo para ver que el test pasa. Ten en cuenta que el hecho de que pase el test no quiere decir que la parte correspondiente esté completamente bien, aunque los tests están diseñados para probar los errores más habituales. Recuerda que si el test es `test_xxx.py`, para ejecutarlo podrás ejecutarlo en PyCharm, o desde la línea de comandos:

```shell
python3 -m unittest tests/test_xxx.py
```

Cuando tengas la práctica lista, puedes realizar una prueba general, incluyendo la comprobación del estilo de acuerdo a PEP8, que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Cómo actualizar el repositorio de la práctica si éste es actualizado

Puede ocurrir que mientras estás trabajando con tu práctica, después de haber hecho tu fork para poder trabajar clonándolo localmente, actualicemos el repositorio plantilla (del que hiciste el fork). Puedes incorporar las modificaciones al repositorio plantilla en el tuyo, de la siguiente manera.

Primero, tienes que añadir, a tu repositorio local (el que clonaste de tu fork) un nuevo `remote`. Los "remote" son los repositorios remotos con los que tu repositorio local puede sincronizarse. Tu repositorio tiene un remote (llamado `origin`) que corresponde con tu repositorio en GitLab (el que creaste como fork del repositorio plantilla). Puedes ver los remotes de tu repositorio:

```shell
git remote -v
```

Verás dos líneas para `origin`, una para `fetch` (recibir datos) y otra para `push` (enviar datos).

Para poder actualizar a partir del repositorio plantilla, vamos a añadir un nuevo remote para él:

```shell
git remote add upstream https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p3-21
```

Si ahora vuelves a ejecutar `git remote -v` verás cuatro líneas, dos para el remote `origin` y dos para el nuevo remote `upstream`.

En este momento es importante comprobar si tienes pendientes modificaciones a ficheros que conoce git (esto es, hay modificaciones que aún no han sido incluidas en ningún commit). Para ello, puedes comprobar que `git status` está limpio, o puedes tratar de realizar un commit que incluya todos los cambios pendintes (si no hay cambios git te lo dirá). Para tratar de haer este commit puedes escribir, en el directorio principal de tu repositorio (atención al `.` final):

```shell
git commit .
```

Ahora que ya tenemos al repositorio plantilla como remote, y estamos seguros de que no hay cambios pendientes, podemos sincronizar con él. Si no has tocado los ficheros que obtuviste del repositorio, git se encargará de gestionar la sincronización completamente. Si los has tocado, puede ser un poco más complicado, pero posible. Por si acaso, antes de realizar los siguientes pasos, copia los ficheros que hayas escrito (tus programas, fundamentalmente) a otro directorio, por si acaso.

La forma más simple de sincronizar los contenidos del repositorio plantilla es la siguiente:

```shell
git fetch upstream
git rebase upstream/master
```

La primera orden bajará la informacion nueva que pueda haber en el remote `upstream` (el reposito plantilla de la practica), pero no la incorporara en el directorio. La segunda orden incorporara los commits nuevos que pueda haber en la rama principal de `upstream` en la rama en la que estés trabajando en tu repo local. Para hacerlo, `git rebase` hará comprobaciones, para asegurarse de que esa incorporación es segura, sin perder información. Además, colocará los commits que tengas pendientes en tu rama detrás de los commits que descargue de `upstream/master`.

Si `git rebase` falla, porque no puede asegurar que la incorporación de commits es segura, te lo dirá, dándote varias opciones, ente ella la de abortar. Si es el caso, trata de entender la situacuón, y en caso de duda, aborta. Probablmente tengas cambios que den conflicto, o tengas cambios si incluir en un commit. Si `git rebase` tiene éxito, ya tienes en tu repositorio local los cambios que se hayan hecho en el repositorio plantilla de la práctica.

Alternativamente a `git rebase` puedes usar `git merge`, que hará algo muy parecido, pero manteniendo la historia de commits de otra forma:

```shell
git fetch upstream
git merge upstream/master
```

Si por algún motivo estás trabajando en una rama distinta de la principal, y quieres incorporar los cambios a la principal, asegúrate que pasas a esa rama principal antes, con `git checkout master`.


## Ejercicio 9 (segundo periodo)

Para realizar esta práctica, tendrás que utilizar el grupo de GitLab que creaste en el último ejercico de la prática 2 (y que llamaste `forks-<user>`). Si no lo has hecho todavía, tendrás que hacer al menos su primera parte (hasta que tengas creado el grupo).

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del [repositorio plantilla de esta práctica](https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p3-21) uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la ETSIT de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `ptavi-p3-<alumno>`.

Crea un clon local de este nuevo repositorio (usando `git clone`), y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Puedes ver el proceso para hacerlo (y para cambiarte de rama) en el enunciado del ultimo ejercico de la práctica 2.

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Añade un nuevo fichero, `karaoke-<user>.smil`, que sea un fichero smil válido (basta con que modifiques levemente el fichero `karaoke.smil`. A continuación, en el directorio `tests` crea un nuevo test, `test_<user>.py` que compruebe que la función `main` del programa `karaoke.py` de `alumno` funciona correctamente, cuando el programa es invocado con la opción `--json` y el nuevo fichero que acabas de crear, `karaoke-<user>.smil`. Esto es, cuando se invoca así:

```shell
karaoke.py --json karaoke-<user>.smil
```

Cuando termines, crea un commit (usando `git add` y `git commit`, o desde Pycharm) que inclya estos cambios. Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes suar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).

# Protocolos para la transmisión de audio y video en Internet
# Práctica 2. Python y GitLab

**Objetivos:** Conocer el funcionamiento básico de git,
y las operaciones típicas que tendremos que realizar con él
durante las prácticas de la asignatura.

**Conocimientos teóricos previos:** Conocimientos básicos
de manejo del intérprete de comandos.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 1 hora.

**Fecha de entrega de la pŕactica:** Esta práctica NO requiere ser entregada

## Introducción

Git es un sistema de control de versiones distribuido,
que permite gestionar los cambios a un conjunto de ficheros (repositorio),
y sincronizarlos con otros repositorios remotos. Lo utilizaremos en las prácticas
de la asignatura en combinación con GitLab, y lo utilizaremos tanto desde el
intérprete de comandos (terminal) como desde el IDE (PyCharm).

## Ejercicio 1

Realiza un fork del repositorio https://gitlab.etsit.urjc.es/ptavi/pruebas
de forma que quede como uno de tus repositorios personales en el GitLab de la
ETSIT.

A continuación, clona este repo que acabas de crear en tu ordenador.
Edita el fichero `README.md` que verás en él, haciendo cualquier cambio, y crea un 
fichero nuevo llamado `fichero`.
Haz un commit que incluya todos estos cambios, y sincroniza el repositorio
que creaste en GitLab para que tenga también este commit.
Comprueba que el repositorio en GitLab tiene los cambios de tu repositorio
local.

## Ejercicio 2

Clona el mismo repositorio que has creado en tu cuenta en GitLab, pero
ahora en un directorio en el laboratorio (o en otro directorio en tu ordenador).
Haz cualquier cambio en este repositorio, y haz un commit que lo incluya.
Sube este commit al repositorio en GitLab, y sinconiza de nuevo tu primer
repositorio local para que tenga este cambio.

## Ejercicio 3

Haz cambios en ambos repositorios locales, en ficheros distintos, y trata de que
aparezcan en el otro repositorio local, y en el repositorio en GitLab.
